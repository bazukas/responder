from .config import conf
from .db import init_db
from .db.question import Question
from .search_models import FaissAvgWord2Vec


class Responder(object):
    """Автоответчик"""

    def __init__(self, initialize_db=True, **kwargs):
        print("Initializing...")
        if initialize_db:
            init_db(conf)
        self.questions = Question.get_questions()
        self.search_model = FaissAvgWord2Vec(self.questions, **kwargs)
        print("Initialization complete")

    def get_questions(self):
        return self.questions

    def search(self, question, k=5, pretty=False):
        """Поиск по базе вопросов"""
        resp = self.search_model.search(question, k)
        if pretty:
            return self.prettify(resp)
        return resp

    def add_question(self, question, answer):
        """Добавление нового вопроса в базу"""
        if self.search_model.add_question(question, answer):
            self.questions.append((question, answer))
            Question.store_question(question, answer)
            return True
        return False

    def prettify(self, triples):
        """Форматирование для показа пользователям"""
        resp = []
        for i, t in enumerate(triples):
            resp.append("%d. Вопрос: %s Ответ: %s Уверенность: %.2f"
                        % (i + 1, t[0], t[1], t[2]))
        return '\n'.join(resp)
