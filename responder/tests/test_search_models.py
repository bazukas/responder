import unittest
from responder.search_models import FaissAvgWord2Vec


class TestSearchModels(unittest.TestCase):

    QUESTIONS = [
        ('Кто ты?', 'Обратитесь в службу поддержки'),
        ('Кто я?', 'Обратитесь в службу поддержки'),
        ('Хочу получить кредитную карту', 'Не надо'),
        ('Хочу получить дебетовую карту', 'Хорошо'),
        ('Что делать?', 'Жить'),
        ('Как быть?', 'Любить'),
    ]

    def setUp(self):
        model_class = FaissAvgWord2Vec
        self.model = FaissAvgWord2Vec(self.QUESTIONS, nlist=1)

    def test_search(self):
        resp = self.model.search('Что делать?')
        self.assertEqual(len(resp), 5)
        self.assertEqual(resp[0][1], 'Жить')

    def test_add_question(self):
        self.model.add_question('Можно пойти гулять?', 'Конечно')
        resp = self.model.search('Можно пойти гулять?')
        self.assertEqual(resp[0][1], 'Конечно')


if __name__ == '__main__':
    unittest.main()
