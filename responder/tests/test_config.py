import unittest
import os

from responder.config import load_config


class TestConfig(unittest.TestCase):
    CONFIG_FILE = 'test_config.ini'
    TEST_CONFIG = """
[DB]
address = sqlite:///:memory:

[Telegram]
token = AAA

[VK]
token = BBB
confirmation_token = CCC

    """

    def setUp(self):
        with open(self.CONFIG_FILE, 'w') as f:
            f.write(self.TEST_CONFIG)

    def tearDown(self):
        os.remove(self.CONFIG_FILE)

    def test_load_config(self):
        conf = load_config(self.CONFIG_FILE)
        self.assertEqual(conf['db.address'], 'sqlite:///:memory:')
        self.assertEqual(conf['telegram.token'], 'AAA')
        self.assertEqual(conf['vk.token'], 'BBB')
        self.assertEqual(conf['vk.confirmation_token'], 'CCC')

if __name__ == '__main__':
    unittest.main()
