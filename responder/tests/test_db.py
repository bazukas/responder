import unittest

from responder.config import conf
from responder.db import init_db
from responder.db.question import Question


class TestDB(unittest.TestCase):

    def setUp(self):
        conf['db.address'] = 'sqlite:///:memory:'
        init_db(conf, create_tables=True)

    def test_questions(self):
        Question.store_question('Вопрос1', 'Ответ1')
        Question.store_question('Вопрос2', 'Ответ2')
        resp = Question.get_questions()
        self.assertEqual(len(resp), 2)
        self.assertEqual(resp[0][1], 'Ответ1')


if __name__ == '__main__':
    unittest.main()
