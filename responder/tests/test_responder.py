import unittest

from responder.responder import Responder
from responder.config import conf
from responder.db import init_db
from responder.db.question import Question


class TestResponder(unittest.TestCase):

    def setUp(self):
        conf['db.address'] = 'sqlite:///:memory:'
        init_db(conf, create_tables=True)
        Question.store_question('Как быть?', 'Ответ1')
        Question.store_question('Что делать?', 'Ответ2')
        self.responder = Responder(initialize_db=False, nlist=1)

    def test_get_questions(self):
        resp = self.responder.get_questions()
        self.assertEqual(len(resp), 2)

    def test_prettify(self):
        triplets = [
            ('Вопрос1?', 'Ответ1.', 1.1),
            ('Вопрос2?', 'Ответ2.', 2.34),
        ]
        resp = self.responder.prettify(triplets)
        self.assertEqual(resp, '1. Вопрос: Вопрос1? Ответ: Ответ1. Уверенность: 1.10\n2. Вопрос: Вопрос2? Ответ: Ответ2. Уверенность: 2.34')


if __name__ == '__main__':
    unittest.main()
