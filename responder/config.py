import configparser


def load_config(config_file):
    """Выгрузка конфига приложения из файла"""
    config = configparser.ConfigParser()
    config.read(config_file)

    return {
        'db.address': config.get('DB', 'address'),
        'telegram.token': config.get('Telegram', 'token'),
        'vk.token': config.get('VK', 'token'),
        'vk.confirmation_token': config.get('VK', 'confirmation_token'),
    }

conf = load_config('data/config.ini')
