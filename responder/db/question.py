import sqlalchemy

from . import Base, Session


class Question(Base):
    """Модель таблицы вопросов"""
    __tablename__ = 'questions'

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    question = sqlalchemy.Column(sqlalchemy.Text)
    answer = sqlalchemy.Column(sqlalchemy.Text)

    @classmethod
    def store_question(cls, question, answer):
        """Сохранение вопроса в таблицу"""
        session = Session()
        session.add(cls(question=question, answer=answer))
        session.commit()

    @classmethod
    def get_questions(cls):
        """Получение списка всех вопросов"""
        session = Session()
        return [(q.question, q.answer) for q in session.query(cls).all()]
