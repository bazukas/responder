from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
Session = sessionmaker()


def init_db(conf, create_tables=False):
    """Инициализация соединения к базе данных"""
    engine = create_engine(conf['db.address'], echo=False)
    Session.configure(bind=engine)

    if create_tables:
        Base.metadata.create_all(engine)
