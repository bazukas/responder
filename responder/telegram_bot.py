from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from .config import conf
from .responder import Responder

r = Responder()


def start(bot, update):
    update.message.reply_text('Приветствую! Это бот автоответчик. Можете задавать вопросы. Автор: Хасаншин Азат')


def search(bot, update):
    update.message.reply_text(r.search(update.message.text, pretty=True))

updater = Updater(conf['telegram.token'])
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(MessageHandler(Filters.text, search))

updater.start_polling()
updater.idle()
