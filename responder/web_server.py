from flask import Flask, request, jsonify
from flask_swagger import swagger
import vk

from .config import conf
from .responder import Responder

app = Flask(__name__)

r = Responder()


@app.route("/search", methods=["GET"])
def search():
    """
    Поиск по базе вопросов
    ---
    tags:
        - search
    produces:
        - "application/json"
    parameters:
        - name: question
          in: query
          description: "Вопрос"
          required: true
          type: "string"
        - name: count
          in: query
          description: "Кол-во возвращаемых троек"
          required: false
          type: "int"
    """
    question = request.args.get('question')
    if not question:
        return "Параметр 'question' обязателен!"
    count = int(request.args.get('count', 5))
    resp = r.search(question, count)
    if not resp:
        return "Не смогли произвести поиск по заданному вопросу"
    return jsonify(resp)


@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['info']['version'] = "0.0.1"
    swag['info']['title'] = "Responder API"
    return jsonify(swag)


session = vk.Session()
api = vk.API(session, v=5.0)


@app.route("/vk", methods=['POST'])
def vk():
    data = request.json
    if 'type' not in data:
        return 'not vk'
    if data['type'] == 'confirmation':
        return conf['vk.confirmation_token']
    elif data['type'] == 'message_new':
        user_id = data['object']['user_id']
        resp = r.search(data['object']['body'], pretty=True)
        api.messages.send(access_token=conf['vk.token'], user_id=str(user_id), message=resp)
    return 'ok'
