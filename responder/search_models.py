import faiss
import nltk
import numpy as np
from gensim.models.keyedvectors import KeyedVectors


class FaissAvgWord2Vec(object):
    """Модель представления вопросов в векторном формате и поиска по базе"""
    DIMS = 100
    NLIST = 100
    NPROBE = 5
    VOCAB_FILE = 'data/word2vec_vocab.bin'

    def __init__(self, questions, dims=None, nlist=None, nprobe=None):
        self.dims = dims or self.DIMS
        self.nlist = nlist or self.NLIST
        self.nprobe = nprobe or self.NPROBE
        self.word_vectors = KeyedVectors.load(self.VOCAB_FILE)

        self.raw_questions = []
        vectorized_questions = []
        for q, a in questions:
            vectorized = self.doc_to_vec(q)
            if vectorized is not None:
                vectorized_questions.append(vectorized)
                self.raw_questions.append((q, a))
        xb = np.concatenate(vectorized_questions)

        self.quantizer = faiss.IndexFlatL2(self.dims)
        self.index = faiss.IndexIVFFlat(self.quantizer, self.dims, self.nlist, faiss.METRIC_L2)
        self.index.train(xb)
        self.index.add(xb)
        self.index.nprobe = self.nprobe

    def doc_to_vec(self, doc):
        """Перевод текста в векторный формат"""
        vectors = [self.word_vectors[w] for w in nltk.word_tokenize(doc) if w in self.word_vectors.vocab]
        if not vectors:
            return None
        return np.average(vectors, axis=0).reshape(1, 100)

    def search(self, question, k=5):
        """Поиск по индексу"""
        vectorized = self.doc_to_vec(question)
        if vectorized is None:
            return []
        D, I = self.index.search(vectorized, k)
        res = []
        for i, s in zip(I[0], D[0]):
            qa = self.raw_questions[i]
            res.append((qa[0], qa[1], float(s)))
        return res

    def add_question(self, question, answer):
        """Добавление нового вопроса в индекс"""
        vectorized = self.doc_to_vec(question)
        if vectorized is not None:
            self.raw_questions.append((question, answer))
            self.index.add(np.matrix(vectorized))
            return True
        return False
